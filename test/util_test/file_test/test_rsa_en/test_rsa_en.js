import { rsaEncrypt } from  "../../../../src/utils/file_utils/rsa_encryption.js"

const pub = "-----BEGIN PUBLIC KEY-----\nMFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAN4ue9UZfm4Oy2Rvo1Im+UWE6aJNrWsm\nPMZkVj3OL3k/ZK1mZZ5A33BZ1In5j/DiFPzkzcIfX00XJ+UrHrxHxKsCAwEAAQ==\n-----END PUBLIC KEY-----"

rsaEncrypt(pub, "E:\\code\\ipfs-based-storage\\test\\util_test\\file_test\\test_rsa_en\\source\\text",
                "E:\\code\\ipfs-based-storage\\test\\util_test\\file_test\\test_rsa_en\\source\\file_en",
                "E:\\code\\ipfs-based-storage\\test\\util_test\\file_test\\test_rsa_en\\source\\file_en\\text_en")
