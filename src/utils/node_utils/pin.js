import { create } from 'kubo-rpc-client'
import { createRequire } from 'module'; 
const require = createRequire(import.meta.url);

var fs = require('fs');

export function pinFileToNode(cid, nodeAddr) {
    const ipfs = create(nodeAddr)
    ipfs.pin(cid)
}
