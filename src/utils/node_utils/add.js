import { create } from 'kubo-rpc-client'
import { createRequire } from 'module'; 
const require = createRequire(import.meta.url);

var fs = require('fs');



export function addFileToNodes(file, nodeAddr) {
    const data = fs.readFileSync(file, "utf-8")
    // console.log(data)

    const ipfs = create(nodeAddr)
    const buf = Buffer.from(data)
    const result = ipfs.add(buf)
    return result.path
}
