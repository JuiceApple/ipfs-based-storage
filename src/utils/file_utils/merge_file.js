import { createRequire } from 'module'; 
const require = createRequire(import.meta.url);
const splitFile = require('split-file');

export function mergeFiles(sourceFiles, targetFile) {
    splitFile.mergeFiles(sourceFiles, targetFile)
    .then(() => {
        console.log('Done!');
    })
    .catch((err) => {
        console.log('Error: ', err);
    });
}