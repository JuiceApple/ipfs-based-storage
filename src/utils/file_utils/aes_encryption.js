import CryptoJS from "crypto-js";
import fs from "fs"

export function aesEncrypt(secret_key, secret_iv, file, targetFolder, targetFile) {
  // 十六位十六进制数作为密钥
  const SECRET_KEY = CryptoJS.enc.Utf8.parse(secret_key);
  // 十六位十六进制数作为密钥偏移量
  const SECRET_IV = CryptoJS.enc.Utf8.parse(secret_iv);

  if (fs.existsSync(targetFolder)) {
    var tempstats = fs.statSync(targetFolder);
    if (!(tempstats.isDirectory())) {
        fs.unlinkSync(targetFolder);
        fs.mkdirSync(targetFolder);
    }
  }
  else{
      fs.mkdirSync(targetFolder);
  }


  const data = fs.readFileSync(file, "utf-8")
  // console.log("file context:", data);
  const dataHex = CryptoJS.enc.Utf8.parse(data);
  const encrypted = CryptoJS.AES.encrypt(dataHex, SECRET_KEY, {
    iv: SECRET_IV,
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.Pkcs7
  });

  fs.writeFileSync(targetFile, encrypted.ciphertext.toString())
}

export function aesDecrypt(secret_key, secret_iv, file, targetFolder, targetFile) {
  // 十六位十六进制数作为密钥
  const SECRET_KEY = CryptoJS.enc.Utf8.parse(secret_key);
  // 十六位十六进制数作为密钥偏移量
  const SECRET_IV = CryptoJS.enc.Utf8.parse(secret_iv);

  if (fs.existsSync(targetFolder)) {
    var tempstats = fs.statSync(targetFolder);
    if (!(tempstats.isDirectory())) {
        fs.unlinkSync(targetFolder);
        fs.mkdirSync(targetFolder);
    }
  }
  else{
      fs.mkdirSync(targetFolder);
  }
    
  const data = fs.readFileSync(file, "utf-8")
  // console.log("file context:", data);
  const encryptedHexStr = CryptoJS.enc.Hex.parse(data);
  const str = CryptoJS.enc.Base64.stringify(encryptedHexStr);
  const decrypt = CryptoJS.AES.decrypt(str, SECRET_KEY, {
    iv: SECRET_IV,
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.Pkcs7
  });
  const decryptedStr = decrypt.toString(CryptoJS.enc.Utf8);

  fs.writeFileSync(targetFile, decryptedStr.toString())

}

export function genKey() {
  const randomHex = () => `${Math.floor(Math.random() * 0xffff).toString(16).padEnd(4, "0")}`;
  return randomHex() + randomHex() + randomHex() + randomHex()
}

export function genIv() {
  const randomHex = () => `${Math.floor(Math.random() * 0xffff).toString(16).padEnd(4, "0")}`;
  return randomHex() + randomHex() + randomHex() + randomHex()
}