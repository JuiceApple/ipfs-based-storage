import CryptoJS from "crypto-js";
import fs from "fs"

export function md5Hash(file) {
  const data = fs.readFileSync(file, "utf-8")
  return CryptoJS.MD5(data).toString()
}

// C:\\Users\\27467\\Desktop\\filesplit\\source\\11.21 ipfs.mp3
// console.log(md5Hash("C:\\Users\\27467\\Desktop\\filesplit\\source\\11.21 ipfs.mp3"))