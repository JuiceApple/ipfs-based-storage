import { createRequire } from 'module'; 
const require = createRequire(import.meta.url);

const SplitFileUtils = require('split-file');
var fs = require('fs');
var path = require('path');


export async function splitFile(sourceFile, targetFolder) {
    if (fs.existsSync(targetFolder)) {
        var tempstats = fs.statSync(targetFolder);
        if (!(tempstats.isDirectory())) {
            fs.unlinkSync(targetFolder);
            fs.mkdirSync(targetFolder);
        }
        const files = await SplitFileUtils.splitFileBySize(sourceFile, 1048576, targetFolder)
        return files.length
    }
    else{
        fs.mkdirSync(targetFolder);
        const files = await SplitFileUtils.splitFileBySize(sourceFile, 1048576, targetFolder)
        return files.length
        
    }
}

// splitFile('C:\\Users\\27467\\Desktop\\filesplit\\source\\11.21 ipfs.mp3', 'C:\\Users\\27467\\Desktop\\filesplit\\source\\storage\\subfiles')