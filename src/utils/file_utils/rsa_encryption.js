import NodeRSA from "node-rsa";
import fs from "fs"

const key = new NodeRSA({b: 512});

var pubkey = key.exportKey('pkcs8-public');//导出公钥
var prikey = key.exportKey('pkcs8-private');//导出私钥
// console.log(pubkey)
// console.log(prikey)

export const rsaEncrypt = (publicKey, file, targetFolder, targetFile) => {
    var pubKey = new NodeRSA(publicKey,'pkcs8-public');
    
    if (fs.existsSync(targetFolder)) {
        var tempstats = fs.statSync(targetFolder);
        if (!(tempstats.isDirectory())) {
            fs.unlinkSync(targetFolder);
            fs.mkdirSync(targetFolder);
        }
      }
      else{
          fs.mkdirSync(targetFolder);
      }

    fs.readFile(file, "utf-8", (err, data) => {
        if (err) console.error(err.message);
        console.log("file context:", data);
        var encrypted = pubKey.encrypt(data, 'base64');

        fs.writeFile(targetFile, encrypted, (err) => {
            if (err) console.error(err.message);
            console.log("encrypte succced");
        })
    })
}

export const rsaDecrypt = (privateKey, file, targetFolder, targetFile) => {
    var priKey = new NodeRSA(privateKey,'pkcs8-private');
    
    if (fs.existsSync(targetFolder)) {
        var tempstats = fs.statSync(targetFolder);
        if (!(tempstats.isDirectory())) {
            fs.unlinkSync(targetFolder);
            fs.mkdirSync(targetFolder);
        }
      }
      else{
          fs.mkdirSync(targetFolder);
      }

    fs.readFile(file, "utf-8", (err, data) => {
        if (err) console.error(err.message);
        console.log("file context:", data);
        var decrypted = priKey.decrypt(data, 'utf-8');
    
        fs.writeFile(targetFile, decrypted, (err) => {
            if (err) console.error(err.message);
            console.log("decrypte succced");
        })
    })

}