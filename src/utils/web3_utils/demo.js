import Web3 from 'web3'

var web3 = new Web3("ws://localhost:8545");
// console.log(web3)
// console.log(Web3.modules)
// console.log(Web3.version)

// web3.eth.getNodeInfo().then(console.log)
// web3.eth.net.isListening().then(console.log)
// web3.eth.net.getId().then(console.log)
// web3.eth.net.getPeerCount().then(console.log)

// console.log(web3.providers)
// console.log(web3.givenProvider)
// console.log(web3.currentProvider)
// console.log(web3.eth.currentProvider)

var transactionObject = {
    from: "0x9b00E6d8AC3ED2e6d3D222B9415c1291e4c05B01",
    to: "0x5fd9b2Ed17c735ad68cefDC384067008fbE2d00F",
    value: web3.utils.toWei('1', 'ether'),
    data: ""
}
web3.eth.sendTransaction(transactionObject).then(console.log);

// var transactionHash = "0x204cd2f55951ab061bf14544005913a6400507a3e25fd6f5a9e6e192fe32495a";
// web3.eth.getTransaction(transactionHash).then(console.log);

// web3.eth.getBalance("0x9b00E6d8AC3ED2e6d3D222B9415c1291e4c05B01", function(error, result) {
//     // var balance = result.toString();
//     console.log(result);
//     // console.log(web3.utils.fromWei(balance, "ether"));
// });
