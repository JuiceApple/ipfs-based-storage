import { createRequire } from 'module'; 
const require = createRequire(import.meta.url);
var fs = require('fs');
var path = require('path');
import { aesEncrypt } from "../utils/file_utils/aes_encryption.js"


export function encryptFile(sub_file_folder, file, sub_enfile_folder, secret_key, secret_iv) {
    aesEncrypt(secret_key, secret_iv, path.join(sub_file_folder, file), sub_enfile_folder, path.join(sub_enfile_folder, file))
}
