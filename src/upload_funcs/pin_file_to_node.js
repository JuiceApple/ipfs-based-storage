import { createRequire } from 'module'; 
import { addFileToNodes } from '../utils/node_utils/add.js';
const require = createRequire(import.meta.url);
var fs = require('fs');
var path = require('path');


export function fixFileToNode(file, nodeAddr) {
    addFileToNodes(file, nodeAddr)
}