// 分割、加密、上传并记录、上传记录

import { splitFile } from  "./utils/file_utils/split_file.js"
import { md5Hash } from "./utils/file_utils/md5.js"
import { createRequire } from 'module'; 
import { getFreeNodes } from "./upload_funcs/get_free_nodes.js";
import { encryptFile } from "./upload_funcs/encrypt_file.js";
import { uploadFileToNodes } from "./upload_funcs/upload_file_to_node.js";
import { fixFileToNode } from "./upload_funcs/pin_file_to_node.js";
import { genIv, genKey } from "./utils/file_utils/aes_encryption.js";
const require = createRequire(import.meta.url);
var fs = require('fs');
var path = require('path');

export async function uploadFile(file_name, source_file, storage_foler) {
    const sub_file_folder = storage_foler + '\\subFiles'
    const sub_enfile_folder = storage_foler + '\\subEnFiles'

    const record = {}
    record.file_name = file_name
    record.hash = md5Hash(source_file)

    // 生成秘钥
    const secret_key = genKey()
    const secret_iv = genIv()

    record.key = secret_key
    record.iv = secret_iv

    // 分割
    const num_of_files = await splitFile(source_file, sub_file_folder)

    //询问空闲节点
    record.nodes = getFreeNodes(num_of_files)

    // 一次循环，加密、上传、记录
    const files = fs.readdirSync(sub_file_folder)
    record.cids = []

    for (let i = 0; i < files.length; i++) {
        const file = files[i]
        // 加密
        encryptFile(sub_file_folder, file, sub_enfile_folder, secret_key, secret_iv)
        // 上传碎片文件并pin

        const cid = uploadFileToNodes(path.join(sub_enfile_folder, file), record.nodes[i * 2])
        fixFileToNode(path.join(sub_enfile_folder, file), record.nodes[i * 2])
        fixFileToNode(path.join(sub_enfile_folder, file), record.nodes[i * 2 + 1])
        
        // 记录
        record.cids[i] = cid
    }

    // 上传记录文件至链上
    console.log(record)
}

const key = "1234123412ABCDEF";  //十六位十六进制数作为密钥
const iv = 'ABCDEF1234123412';   //十六位十六进制数作为密钥偏移量

uploadFile('11.21 ipfs.mp3', 'C:\\Users\\27467\\Desktop\\filesplit\\source\\11.21 ipfs.mp3', 
'C:\\Users\\27467\\Desktop\\filesplit\\source')
